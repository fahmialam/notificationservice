package id.ist.notificationservicev4.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {


    @Bean
    public ObjectMapper mapper() {
        return new ObjectMapper();
    }
}
