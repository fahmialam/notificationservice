package id.ist.notificationservicev4.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.ist.notificationservicev4.model.EmailModel;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class MessageListenerService {

    private final ObjectMapper mapper;

    @Autowired
    public MessageListenerService(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @RabbitListener(queues = "learning-microservice")
    public void sendEmail(String message) {
        log.info("Receiving message: " + message);
        try {
            EmailModel email = mapper.readValue(message, EmailModel.class);
            log.info("Converting to model...");
            log.info("EmailModel:: " + email.toString());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
