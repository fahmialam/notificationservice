package id.ist.notificationservicev4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotificationServicev4Application {

    public static void main(String[] args) {
        SpringApplication.run(NotificationServicev4Application.class, args);
    }

}
